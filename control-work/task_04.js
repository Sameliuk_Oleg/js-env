/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Знайти різницю максимального та мінімального значень поля `value`.
 */

const {graph} = require('./graph');

const minMaxDiff = graph => {
    // TODO: Place your solution here!
    const objCompare = (obj1, obj2) => {
        let newObj = {};
        if (obj1.max > obj2.max) {
            newObj.max = obj1.max
        } else {
            newObj.max = obj2.max
        }
        if (obj1.min < obj2.min) {
            newObj.min = obj1.min
        } else {
            newObj.min = obj2.min
        }
        return newObj
    }

    const traversalTree = (graphs) => {
        return graphs.reduce((accumulator, currentValue) => {
            if (accumulator.max === 0) {
                accumulator.max = currentValue.value;
            }
            if (accumulator.min === 0) {
                accumulator.min = currentValue.value
            }
            if (currentValue.value > accumulator.max) {
                accumulator.max = currentValue.value
            }
            if (currentValue.value < accumulator.min) {
                accumulator.min = currentValue.value
            }
            if (currentValue.children) {
                let newObj = traversalTree(currentValue.children);
                accumulator = objCompare(accumulator, newObj)
            }
            return accumulator;
        }, {min: 0, max: 0})
    };

    let result = traversalTree([graph]);

    return result.max - result.min
};

const checks = [
    minMaxDiff({value: 1}) === 0, // 1 - 1
    minMaxDiff({value: 1, children: [{value: 2}]}) === 1, // 2 - 1
    minMaxDiff({value: 1, children: [{value: 2}, {value: 3}]}) === 2, // 3 - 1
    minMaxDiff({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 3, // 4 - 1
    minMaxDiff(graph) === 100
];

console.group('Task 04');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};
