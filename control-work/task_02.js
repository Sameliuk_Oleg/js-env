/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Порахувати кількість листків - нод без дітей.
 */

const {graph} = require('./graph');

const leafs = graph => {
    const traversalTree = (graphs) => {
        return graphs.reduce((accumulator, currentValue) => {
            if (currentValue.children) {
                accumulator += traversalTree(currentValue.children)
            } else {
                accumulator += 1
            }
            return accumulator;
        }, 0)
    };

    return traversalTree([graph])
};

const checks = [
    leafs({value: 1}) === 1,
    leafs({value: 1, children: [{value: 2}]}) === 1,
    leafs({value: 1, children: [{value: 2}, {value: 3}]}) === 2,
    leafs({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 2,
    leafs(graph) === 269
];

console.group('Task 02');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};