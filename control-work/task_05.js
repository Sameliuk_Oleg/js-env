/*
    Розвязати, так звану, "задачу равлика", або "задачу спіралі". Реалізувати функцію `snail`.
    Суть задачі полягає в тому, що з 2-вимірного масиву робиться 1-вимірний, ніби розмотуючи масив.
    При чому, парамерт clockwise <boolean> вказує напрямок, а параметр start <number: 1-4> вказує,
    з якої вершини починається обхід: 1 - верхня ліва, 2 - верхня права, 3 - нижня права, 4 - нижня ліва.

    Схематично це можна відоразити так:

          clockwise = true
       1                   2
        +-----------------+
        +---------------+ |
        | +-----------> | |
        | +-------------+ |
        +-----------------+
       4                   3


          clockwise = false
       1                   2
        + +---------------+
        | | +-----------+ |
        | | +---------> | |
        | +-------------+ |
        +-----------------+
       4                   3
*/

const a0x0 = [];

const a1x1 = [['a']];

const a1x5 = [['a', 'b', 'c', 'd', 'e']];

const a5x1 = [
    ['a'],
    ['b'],
    ['c'],
    ['d'],
    ['e']
];

const a2x3 = [
    ['a', 'b', 'c'],
    ['d', 'e', 'f']
];

const a4x4 = [
    ['a', 'b', 'c', 'd'],
    ['e', 'f', 'g', 'h'],
    ['i', 'j', 'k', 'l'],
    ['m', 'n', 'o', 'p']
];

const snail = (array, clockwise = true, start = 1) => {
    // TODO: Place your solution here!


};


const checker = (a1, a2) => {
    try {
        if (!Array.isArray(a1) || !Array.isArray(a2) || a1.length !== a2.length) {
            return false;
        }
        for (let i = 0; i < a1.length; i++) {
            if (a1[i] !== a2[i]) {
                return false;
            }
        }
    } catch (e) {
        return false;
    }

    return true;
};

const checks = [
    checker(snail(a0x0), []),

    checker(snail(a1x1), ['a']),

    checker(snail(a1x5), ['a', 'b', 'c', 'd', 'e']),

    checker(snail(a5x1), ['a', 'b', 'c', 'd', 'e']),

    checker(snail(a2x3), ['a', 'b', 'c', 'f', 'e', 'd']),
    checker(snail(a2x3, true), ['a', 'b', 'c', 'f', 'e', 'd']),
    checker(snail(a2x3, true, 1), ['a', 'b', 'c', 'f', 'e', 'd']),
    checker(snail(a2x3, true, 2), ['c', 'f', 'e', 'd', 'a', 'b']),
    checker(snail(a2x3, true, 3), ['f', 'e', 'd', 'a', 'b', 'c']),
    checker(snail(a2x3, true, 4), ['d', 'a', 'b', 'c', 'f', 'e']),
    checker(snail(a2x3, false), ['a', 'd', 'e', 'f', 'c', 'b']),
    checker(snail(a2x3, false, 1), ['a', 'd', 'e', 'f', 'c', 'b']),
    checker(snail(a2x3, false, 2), ['c', 'b', 'a', 'd', 'e', 'f']),
    checker(snail(a2x3, false, 3), ['f', 'c', 'b', 'a', 'd', 'e']),
    checker(snail(a2x3, false, 4), ['d', 'e', 'f', 'c', 'b', 'a']),

    checker(snail(a4x4), ['a', 'b', 'c', 'd', 'h', 'l', 'p', 'o', 'n', 'm', 'i', 'e', 'f', 'g', 'k', 'j']),
    checker(snail(a4x4, true), ['a', 'b', 'c', 'd', 'h', 'l', 'p', 'o', 'n', 'm', 'i', 'e', 'f', 'g', 'k', 'j']),
    checker(snail(a4x4, true, 1), ['a', 'b', 'c', 'd', 'h', 'l', 'p', 'o', 'n', 'm', 'i', 'e', 'f', 'g', 'k', 'j']),
    checker(snail(a4x4, true, 2), ['d', 'h', 'l', 'p', 'o', 'n', 'm', 'i', 'e', 'a', 'b', 'c', 'g', 'k', 'j', 'f']),
    checker(snail(a4x4, true, 3), ['p', 'o', 'n', 'm', 'i', 'e', 'a', 'b', 'c', 'd', 'h', 'l', 'k', 'j', 'f', 'g']),
    checker(snail(a4x4, true, 4), ['m', 'i', 'e', 'a', 'b', 'c', 'd', 'h', 'l', 'p', 'o', 'n', 'j', 'f', 'g', 'k']),
    checker(snail(a4x4, false), ['a', 'e', 'i', 'm', 'n', 'o', 'p', 'l', 'h', 'd', 'c', 'b', 'f', 'j', 'k', 'g']),
    checker(snail(a4x4, false, 1), ['a', 'e', 'i', 'm', 'n', 'o', 'p', 'l', 'h', 'd', 'c', 'b', 'f', 'j', 'k', 'g']),
    checker(snail(a4x4, false, 2), ['d', 'c', 'b', 'a', 'e', 'i', 'm', 'n', 'o', 'p', 'l', 'h', 'g', 'f', 'j', 'k']),
    checker(snail(a4x4, false, 3), ['p', 'l', 'h', 'd', 'c', 'b', 'a', 'e', 'i', 'm', 'n', 'o', 'k', 'g', 'f', 'j']),
    checker(snail(a4x4, false, 4), ['m', 'n', 'o', 'p', 'l', 'h', 'd', 'c', 'b', 'a', 'e', 'i', 'j', 'k', 'g', 'f']),
];

console.group('Task 05');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};
