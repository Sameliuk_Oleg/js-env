/*
    Реалізувати функцію `circles`, яка:
    - створює прямокутний масив (матрицю), де h - висота (к-ть рядків) та w - ширина (к-ть стовпців);
    - заповнює масив числами, які зростають від країв до центру, починаючи з 1.

    Наприклад, при h = 5, та w = 8.
    [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 2, 2, 2, 2, 2, 2, 1],
        [1, 2, 3, 3, 3, 3, 2, 1],
        [1, 2, 2, 2, 2, 2, 2, 1],
        [1, 1, 1, 1, 1, 1, 1, 1]
    ]
*/


const a0x0 = [];

const a1x1 = [[1]];

const a1x5 = [[1, 1, 1, 1, 1]];

const a5x1 = [
    [1],
    [1],
    [1],
    [1],
    [1]
];

const a2x3 = [
    [1, 1, 1],
    [1, 1, 1]
];

const a3x8 = [
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 2, 2, 2, 2, 2, 2, 1],
    [1, 1, 1, 1, 1, 1, 1, 1]
];

const a4x4 = [
    [1, 1, 1, 1],
    [1, 2, 2, 1],
    [1, 2, 2, 1],
    [1, 1, 1, 1]
];

const a5x6 = [
    [1, 1, 1, 1, 1, 1],
    [1, 2, 2, 2, 2, 1],
    [1, 2, 3, 3, 2, 1],
    [1, 2, 2, 2, 2, 1],
    [1, 1, 1, 1, 1, 1]
];


const circles = (h, w) => {
    // TODO: Place your solution here!
    let min;
    let minI;
    if (h < w) {
        min = h
    } else {
        min = w
    }

    let inferiorityRow = Math.ceil(min / 2);
    let inferiorityRow2 = Math.floor(h / 2);

    const calcRow = (indexRow, w) => {
        let row = [];
        let lastIndex = w - 1;
        if ((indexRow + 1) < w) {
            minI = (indexRow + 1)
        } else {
            minI = w
        }
        for (let i = 0; i < minI; i++) {
            row[i] = i + 1
            if ((lastIndex - i) >= 0) {
                row[lastIndex - i] = i + 1
            }
        }

        for (let i = 0; i < w; i++) {
            if (!row[i]) {
                row[i] = (indexRow + 1)
            }
        }
        return row
    }

    let arr = [];
    for (let i = 0; i < h; i++) {
        if (i >= inferiorityRow2) {
            arr.push(calcRow((h - i - 1), w))
        } else {
            arr.push(calcRow(i, w))
        }
    }
    console.log(arr, inferiorityRow)
    return arr

};

const checker = (a1, a2) => {
    try {
        if (!Array.isArray(a1) || !Array.isArray(a2) || a1.length !== a2.length) {
            return false;
        }

        for (let i = 0; i < a1.length; i++) {
            if (a1[i].length !== a2[i].length) {
                return false;
            }

            for (let j = 0; j < a1[i].length; j++) {
                if (a1[i][j] !== a2[i][j]) {
                    return false;
                }
            }
        }
    } catch (e) {
        return false;
    }

    return true;
};

const checks = [
    checker(circles(0, 0), a0x0),
    checker(circles(1, 1), a1x1),
    checker(circles(1, 5), a1x5),
    checker(circles(5, 1), a5x1),
    checker(circles(2, 3), a2x3),
    checker(circles(3, 8), a3x8),
    checker(circles(4, 4), a4x4),
    checker(circles(5, 6), a5x6),
];

console.group('Task 01');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};