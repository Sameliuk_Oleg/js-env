const {checks: checks_1} = require('./task_01');
const {checks: checks_2} = require('./task_02');
const {checks: checks_3} = require('./task_03');
const {checks: checks_4} = require('./task_04');
const {checks: checks_5} = require('./task_05');

const checks_all = [checks_1, checks_2, checks_3, checks_4, checks_5];

console.group('All Checks');
console.log(checks_all.filter(checks => checks.filter(i => i).length === checks.length).length);
console.groupEnd();