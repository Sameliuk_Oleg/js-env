/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Знайти максимальну глибину дерева - довжину найдовшої гілки, рахуючи корінь за 0
 */

const {graph} = require('./graph');

const depth = graph => {
    // TODO: Place your solution here!
    const traversalTree = (graphs, defaultValue) => {
        return graphs.reduce((accumulator, currentValue) => {
            if (currentValue.children) {
                let count = traversalTree(currentValue.children, defaultValue+1)
                if (accumulator <= count) {
                    accumulator = count
                }
            }
            return accumulator;
        }, defaultValue)
    };

    return traversalTree([graph], 0)
};

const checks = [
    depth({value: 1}) === 0,
    depth({value: 1, children: [{value: 2}]}) === 1,
    depth({value: 1, children: [{value: 2}, {value: 3}]}) === 1,
    depth({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 2,
    depth(graph) === 10
];

console.group('Task 03');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};