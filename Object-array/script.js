function unique(arr) {
    return Array.from(new Set(arr));
}

const arrayLanguage = data.map((x, index) => {
    return data[index][7];
});

let arrayUniqueLanguage = unique(arrayLanguage);
console.log(arrayUniqueLanguage);

checkCategory = (attribute, value, category) => (value[7] === attribute && value[15] === category) ? 1 : 0

getCount = (attribute, category) => {
    return data.reduce((accum, value) => {return (accum + checkCategory(attribute, value, category))}, 0 )
}

customeArray = arrayUniqueLanguage.map((attribute) => {
    let first   = getCount(attribute, 'до 10 человек')
    let second   = getCount(attribute, 'до 50 человек')
    let third   = getCount(attribute, 'до 200 человек')
    let fourth   = getCount(attribute, 'до 1000 человек')
    let fifth   = getCount(attribute, 'свыше 1000 человек')

    let sum = (first + second + third + fourth + fifth) / 100;
    return[attribute, first / sum, second / sum, third / sum, fourth / sum, fifth / sum]
})

/**
 * graph
 */
const SCALE = 15;
const createElement = (qualifierName, parentNode) => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', qualifierName);
    parentNode.appendChild(element);
    return element;
};

const setAttributes = (node, ...attributes) => {
    attributes.forEach(item => {
        Object.entries(item).forEach(([key, value]) => {
            node.setAttribute(key, value);
        });
    });
};

const svg = createElement('svg', document.body);
setAttributes(svg, {
    width: 100 * SCALE,
    height: 100 * SCALE,
    viewBox: '0 0 120 180'
});

const lineX = createElement('rect', svg);
setAttributes(lineX, {
    id: 'lineX',
    fill: '#111111',
    x: 10,
    width: 0.1,
    height: 110
});

const lineY = createElement('rect', svg);
setAttributes(lineY, {
    id: 'lineY',
    fill: '#111111',
    x: 10,
    y: 110,
    width: 120,
    height: 0.1

});

for (let i = 0; i < arrayUniqueLanguage.length; i++) {
    const fifth = createElement('rect', svg);
    setAttributes(fifth, {
        id: 'fifth',
        fill: '#683232',
        x: 11 + (i * 3),
        y: 110 - customeArray[i][1] - customeArray[i][2] - customeArray[i][3] - customeArray[i][4] - customeArray[i][5],
        width: 2,
        height: customeArray[i][1] + customeArray[i][2] + customeArray[i][3] + customeArray[i][4] + customeArray[i][5],

    });

    const fourth = createElement('rect', svg);
    setAttributes(fourth, {
        id: 'fourth',
        fill: '#6B3E3E',
        x: 11 + (i * 3),
        y: 110 - customeArray[i][1] - customeArray[i][2] - customeArray[i][3] - customeArray[i][4],
        width: 2,
        height: customeArray[i][1] + customeArray[i][2] + customeArray[i][3] + customeArray[i][4],
    });

    const third = createElement('rect', svg);
    setAttributes(third, {
        id: 'third',
        fill: '#956A6A',
        x: 11 + (i * 3),
        y: 110 - customeArray[i][1] - customeArray[i][2] - customeArray[i][3],
        width: 2,
        height: customeArray[i][1] + customeArray[i][2] + customeArray[i][3],
    });

    const second = createElement('rect', svg);
    setAttributes(second, {
        id: 'second',
        fill: '#B18D8D',
        x: 11 + (i * 3),
        y: 110 - customeArray[i][1] - customeArray[i][2],
        width: 2,
        height: customeArray[i][1] + customeArray[i][2],
    });

    const first = createElement('rect', svg);
    setAttributes(first, {
        id: 'first',
        fill: '#D6B9B9',
        x: 11 + (i * 3),
        y: 110 - customeArray[i][1],
        width: 2,
        height: customeArray[i][1],
    });

    const lineGraphX = createElement('rect', svg);
    setAttributes(lineGraphX, {
        id: 'lineGraphX',
        fill: '#111111',
        x: 9.5,
        y: 110 - 10 * [i],
        width: 1,
        height: 0.2,
    });

    const textNumber = createElement('text', svg);
    textNumber.append(i + 1);
    setAttributes(textNumber, {
        id: 'textNumber',
        fill: '#111111',
        x: 11.7 + 3 * i,
        y: 112,
        width: 14,
        height: 14,
    });

    const textLanguage = document.createElement('li');
    let text = document.createTextNode(i + 1 + ' - ' + arrayUniqueLanguage[i]);
    textLanguage.appendChild(text);
    document.getElementById('root').appendChild(textLanguage);
}

for (let i = 10; i >= 0; i--) {

    const languagePercent = document.createElement('p');
    let languagePercentText = document.createTextNode(i * 10 + '%');
    languagePercent.appendChild(languagePercentText);
    document.getElementById('percent').appendChild(languagePercent);
}

const textLanguageColorFirst = document.createElement('li');
let textFirst = document.createTextNode('до 10 человек -');
textLanguageColorFirst.appendChild(textFirst);
document.getElementById('history').appendChild(textLanguageColorFirst);

const firstColor = createElement('rect', svg);
setAttributes(firstColor, {
    id: 'firstColor',
    fill: '#D6B9B9',
    x: -12,
    y: 54,
    width: 2,
    height: 2,
});

const textLanguageColorSecond = document.createElement('li');
let textSecond = document.createTextNode('до 50 человек -');
textLanguageColorSecond.appendChild(textSecond);
document.getElementById('history').appendChild(textLanguageColorSecond);

const secondColor = createElement('rect', svg);
setAttributes(secondColor, {
    id: 'secondColor',
    fill: '#B18D8D',
    x: -12,
    y: 58,
    width: 2,
    height: 2,
});

const textLanguageColorThird = document.createElement('li');
let textThird = document.createTextNode('до 200 человек -');
textLanguageColorThird.appendChild(textThird);
document.getElementById('history').appendChild(textLanguageColorThird);

const thirdColor = createElement('rect', svg);
setAttributes(thirdColor, {
    id: 'thirdColor',
    fill: '#956A6A',
    x: -12,
    y: 62,
    width: 2,
    height: 2,
});

const textLanguageColorFourth = document.createElement('li');
let textFourth = document.createTextNode('до 1000 человек -');
textLanguageColorFourth.appendChild(textFourth);
document.getElementById('history').appendChild(textLanguageColorFourth);

const fourthColor = createElement('rect', svg);
setAttributes(fourthColor, {
    id: 'fourthColor',
    fill: '#6B3E3E',
    x: -12,
    y: 66,
    width: 2,
    height: 2,
});

const textLanguageColorFifth = document.createElement('li');
let textFifth = document.createTextNode('свыше 1000 человек -');
textLanguageColorFifth.appendChild(textFifth);
document.getElementById('history').appendChild(textLanguageColorFifth);

const fifthColor = createElement('rect', svg);
setAttributes(fifthColor, {
    id: 'fifthColor',
    fill: '#683232',
    x: -12,
    y: 70,
    width: 2,
    height: 2,
});