import Point from './point';
import Line from './line';

export default class Rectangle {
    point1: { x:number; y:number };
    point2:{ x:number; y:number };
    point:{ x:number; y:number };

    constructor({point1, point2}: { point1: { x:number; y:number }, point2: { x:number; y:number } }) {
        this.point1 = point1;
        this.point2 = point2;
    }

    isSquare() {
        const lb = new Point(this.left, this.bottom);
        const lt = new Point(this.left, this.top);
        const rb = new Point(this.right, this.bottom);
        return new Line(lb, lt).length === new Line(lb, rb).length;
    }

    contains({point}: { point: { x:number; y:number } }) {
        return point.x > this.left &&
            point.x < this.right &&
            point.y > this.bottom &&
            point.y < this.top;
    }

    get left() {
        return Math.min(this.point1.x, this.point2.x);
    }

    get right() {
        return Math.max(this.point1.x, this.point2.x);
    }

    get bottom() {
        return Math.min(this.point1.y, this.point2.y);
    }

    get top() {
        return Math.max(this.point1.y, this.point2.y);
    }

    get width() {
        return this.right - this.left;
    }

    get height() {
        return this.top - this.bottom;
    }

    get area() {
        return this.width * this.height;
    }

    intersect(rectangle: {left:number, right: number, top: number, bottom: number}) {
        const
            left = Math.max(rectangle.left, this.left),
            right = Math.min(rectangle.right, this.right),
            top = Math.min(rectangle.top, this.top),
            bottom = Math.max(rectangle.bottom, this.bottom);

        if (right > left && top > bottom) {
            return new Rectangle({point1: new Point(left, bottom), point2: new Point(right, top)});
        }
    }
}