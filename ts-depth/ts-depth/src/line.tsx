export default class Line {

    start: { x:number, y:number };
    end: { x:number; y:number };

    constructor(start:{ x:number, y:number }, end:{ x:number, y:number }) {
        this.start = start;
        this.end = end;
    }

    get length() {
        return Math.hypot(
            this.start.x - this.end.x,
            this.start.y - this.end.y
        );
    }
}