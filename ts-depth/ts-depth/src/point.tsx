export default class Point {
    x: number;
    y: number;


    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    static randomNumber({from, to}: { from: number, to: number }) {
        return Math.random() * (to - from) + from;
    }

    static randomPoint({xFrom, xTo, yFrom, yTo}: { xFrom: number, xTo: number, yFrom: number, yTo: number }) {
        return new Point(
            Point.randomNumber({from: xFrom, to: xTo}),
            Point.randomNumber({from: yFrom, to: yTo})
        );
    }
}