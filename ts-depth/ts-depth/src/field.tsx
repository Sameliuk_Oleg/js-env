export default class Field {
    ctx: CanvasRenderingContext2D;


    static get WIDTH() {
        return 800;
    }

    static get HEIGHT() {
        return 500;
    }

    constructor(ctx:CanvasRenderingContext2D) {
        this.ctx = ctx;
    }

    strokeRectangle({rectangle}: { rectangle: { left:number, top:number, width:number, height:number } }) {
        this.ctx.strokeStyle = '#595959';
        this.ctx.lineWidth = 2;
        this.ctx.strokeRect(rectangle.left, rectangle.top, rectangle.width, -rectangle.height);
    }

    fillDot(point: { x:number, y: number }) {
        this.ctx.beginPath();
        this.ctx.arc(point.x, point.y, 4, 0, 2 * Math.PI);
        this.ctx.fillStyle = 'red';
        this.ctx.fill();
    }

    strokeEllipse(rectangle: { left: number, width: number, bottom: number, height: number}) {
        const x = rectangle.left + rectangle.width / 2;
        const y = rectangle.bottom + rectangle.height / 2;

        this.ctx.beginPath();
        this.ctx.ellipse(x, y, rectangle.width / 2, rectangle.height / 2, Math.PI, 0, 2 * Math.PI);
        this.ctx.stroke();
    }
}