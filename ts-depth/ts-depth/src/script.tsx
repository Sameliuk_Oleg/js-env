import Field from './field';
import Rectangle from './rectangle';
import Ellipse from './ellipse';
import Point from './point';


    const field = new Field( document.getElementById('canvas').getContext('2d'));




    const rectangle1 = new Rectangle(
        {
            point1: Point.randomPoint({xFrom: 0, xTo: Field.WIDTH, yFrom: 0, yTo: Field.HEIGHT}),
            point2: Point.randomPoint({xFrom: 0, xTo: Field.WIDTH, yFrom: 0, yTo: Field.HEIGHT})
        }
    );
    const rectangle2 = new Rectangle(
        {
            point1: Point.randomPoint({xFrom: 0, xTo: Field.WIDTH, yFrom: 0, yTo: Field.HEIGHT}),
            point2: Point.randomPoint({xFrom: 0, xTo: Field.WIDTH, yFrom: 0, yTo: Field.HEIGHT})
        }
    );

    field.strokeRectangle({rectangle: rectangle1});
    field.strokeRectangle({rectangle: rectangle2});

    const rectangle = rectangle2.intersect({bottom: 0, left: 0, right: 0, top: 0});

    if (rectangle) {
        field.strokeEllipse({bottom: 0, height: 0, left: 0, width: 0});

        let array = [];

        let radiusX = (rectangle.right - rectangle.left) / 2;
        let radiusY = (rectangle.top - rectangle.bottom) / 2;
        let ellipse = new Ellipse(
            new Point(rectangle.left + radiusX, rectangle.bottom + radiusY),
            radiusX,
            radiusY
        );

        while (array.length < 1000) {
            let point = Point.randomPoint(
                {xFrom: rectangle.left, xTo: rectangle.right, yFrom: rectangle.bottom, yTo: rectangle.top}
            );

            if (ellipse.contains({x: 0, y: 0})) {
                array.push(point);
            }
        }

        array.forEach(point => field.fillDot({x: 0, y: 0}));
    }

