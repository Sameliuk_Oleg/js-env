

export default class Ellipse {
    center: { x:number, y: number };
    radius1: number;
    radius2: number;

    constructor(center: { x:number, y: number }, radius1: number, radius2: number) {
        this.center = center;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    isCircle() {
        return this.radius1 === this.radius2;
    }

    contains(point: { x: number, y: number }) {
        return (point.x - this.center.x) ** 2 / this.radius1 ** 2 + (point.y - this.center.y) ** 2 / this.radius2 ** 2 < 1;
    }

    get area() {
        return Math.PI * this.radius1 * this.radius2;
    }
}