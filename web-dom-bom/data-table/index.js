import laureate from "../laureate.js";

export default class DataTable {
    #parent;
    #data;
    #currentPage = 1;
    #num = 10;
    #table = document.createElement('table');
    #tbody = document.createElement('tbody')
    #tr = document.createElement('tr')
    #select = document.createElement('select');
    #pagination = document.createElement('div')

    constructor(parent) {
        this.#parent = parent;
    }

    set data(data) {
        this.#data = data;
    }


    #blockpagination = (num) => {
        this.#pagination.textContent = '';

        for (let i = 0; i < this.#data.length / num; i++) {
            let btnPagination = document.createElement('button');
            btnPagination.setAttribute('class', 'btn-pagination' + i);
            btnPagination.setAttribute('value', i.toString())
            btnPagination.textContent = i;
            this.#pagination.appendChild(btnPagination)
            document.body.appendChild(this.#pagination);
            btnPagination.addEventListener("click", (e) => {
                this.#jumpToPage(e.target.value)
            });
        }
    }

    #changeNumberLine = (selectOption) => {
        this.#num = selectOption;
        this.#drawPage(this.#currentPage, this.#num);
    }

    #jumpToPage = (i) => {
        this.#tr.textContent = '';
        this.#tbody.textContent = '';
        this.#drawPage(i, this.#num);
    }

    #drawPage = (page, num) => {
        this.#blockpagination(num);
        this.#table.className = 'table';
        this.#tr.textContent = '';
        this.#tbody.textContent = '';
        this.#tr.appendChild(this.#tbody)
        this.#table.appendChild(this.#tbody)

        let labelId = document.createElement('td');
        let labelFirstName = document.createElement('td');
        let labelLastName = document.createElement('td');
        let labelBornLocation = document.createElement('td');
        let labelDiedLocation = document.createElement('td');
        let labelGender = document.createElement('td');
        let labelPrizes = document.createElement('td');
        let labelBorn = document.createElement('td');
        let labelDied = document.createElement('td');

        labelId.textContent = '#';
        labelId.addEventListener("click", () => {this.#sortNum('#')})
        labelFirstName.textContent = 'First Name';
        labelFirstName.addEventListener("click", () => {this.#sortLater('First Name')})
        labelLastName.textContent = 'Last Name';
        labelLastName.addEventListener("click", () => {this.#sortLater('Last Name')})
        labelBornLocation.textContent = 'Born Location';
        labelBornLocation.addEventListener("click", () => {this.#sortLater('Born Location')})
        labelDiedLocation.textContent = 'Died Location';
        labelDiedLocation.addEventListener("click", () => {this.#sortLater('Died Location')})
        labelGender.textContent = 'Gender';
        labelGender.addEventListener("click", () => {this.#sortLater('Gender')})
        labelPrizes.textContent = 'Prizes';
        labelPrizes.addEventListener("click", () => {this.#sortLater('Prizes')})
        labelBorn.textContent = 'Born';
        labelBorn.addEventListener("click", () => {this.#sortNum('Born')})
        labelDied.textContent = 'Died';
        labelDied.addEventListener("click", () => {this.#sortNum('Died')})


        this.#tr.appendChild(labelId);
        this.#tr.appendChild(labelFirstName);
        this.#tr.appendChild(labelLastName);
        this.#tr.appendChild(labelBornLocation)
        this.#tr.appendChild(labelDiedLocation);
        this.#tr.appendChild(labelGender);
        this.#tr.appendChild(labelPrizes);
        this.#tr.appendChild(labelBorn);
        this.#tr.appendChild(labelDied)

        this.#tbody.appendChild(this.#tr);

        this.#data.forEach((currentValue, index) => {
            if (index >= (page - 1) * num && index < page * num) {

                let tr = document.createElement('tr');
                const idUsers = document.createElement('td');
                let firstName = document.createElement('td');
                let lastName = document.createElement('td');
                let bornLocation = document.createElement('td');
                let diedLocation = document.createElement('td');
                let gender = document.createElement('td');
                let prizes = document.createElement('td');
                let born = document.createElement('td');
                let died = document.createElement('td');

                idUsers.textContent = currentValue['#'];
                firstName.textContent = currentValue['First Name'] ;
                lastName.textContent = currentValue['Last Name'];
                bornLocation.textContent = currentValue['Born Location'];
                diedLocation.textContent = currentValue['Died Location'];
                gender.textContent = currentValue['Gender'];
                prizes.textContent = currentValue['Prizes'];
                born.textContent = currentValue['Born'];
                died.textContent = currentValue['Died'];

                tr.appendChild(idUsers);
                tr.appendChild(firstName);
                tr.appendChild(lastName);
                tr.appendChild(bornLocation);
                tr.appendChild(diedLocation);
                tr.appendChild(gender);
                tr.appendChild(prizes);
                tr.appendChild(born);
                tr.appendChild(died);

                let ID = document.createElement('td');
                let Name = document.createElement('td');

                ID.textContent = currentValue['ID'];
                Name.textContent = currentValue['Name'] ;

                tr.appendChild(ID);
                tr.appendChild(Name);


                this.#tbody.appendChild(tr);
            }
            this.#table.appendChild(this.#tbody)
            document.body.appendChild(this.#table)
        })
    }


    #sortNum = (value) => {
        this.#data = this.#data.sort((a, b) => (a[value] - b[value]))
        this.#drawPage(this.#currentPage, this.#num);
    }

    #sortLater = (value) => {
        this.#data = this.#data.sort((a,b) =>{
            let nameA = a[value]
            let nameB = b[value]


            if (nameA < nameB ) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            return 0;
        } )
        this.#drawPage(this.#currentPage, this.#num);
    }



    build() {
        this.#blockpagination(this.#num)
        let optionFive = document.createElement('option');
        let optionTen = document.createElement('option');
        let optionTwentyFive = document.createElement('option');
        let optionFifty = document.createElement('option');
        let optionOneHundred = document.createElement('option');
        let optionTwoHundredFifty = document.createElement('option');

        optionFive.value = '5';
        optionTen.value = '10';
        optionTwentyFive.value = '25';
        optionFifty.value = '50';
        optionOneHundred.value = '100';
        optionTwoHundredFifty.value = '250';

        optionTen.setAttribute('selected', 'selected');

        optionFive.textContent = '5';
        optionTen.textContent = '10';
        optionTwentyFive.textContent = '25';
        optionFifty.textContent = '50';
        optionOneHundred.textContent = '100';
        optionTwoHundredFifty.textContent = '250';

        this.#select.appendChild(optionFive);
        this.#select.appendChild(optionTen);
        this.#select.appendChild(optionTwentyFive);
        this.#select.appendChild(optionFifty);
        this.#select.appendChild(optionOneHundred);
        this.#select.appendChild(optionTwoHundredFifty);

        this.#parent.appendChild(this.#select);
        this.#select.addEventListener("change", (e) => {
            this.#changeNumberLine(e.target.value);
        })

        this.#drawPage(this.#currentPage, this.#num);
    }

}