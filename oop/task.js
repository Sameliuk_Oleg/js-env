import Validator from './validator.js';
export default class Task {
    constructor(ID, name, description, status, estimate, loggedWork, assignee) {
        Validator.validateTaskString(name,'name');
        Validator.validateTaskString(description,'description');
        Validator.validateTaskString(status,'status');
        Validator.validateTaskEstimateNumber(estimate, 'estimate');
        Validator.validateTaskLoggedWork(loggedWork, 'logged work')


        this.ID = (new Date()).getTime();
        this.name = name;
        this.description = description;
        this.status = status;
        this.estimate = estimate;
        this.loggedWork = loggedWork;
        this.assignee = assignee;
    }

    assignDeveloper(developer) {
        this.assignee = developer.firstName + ' ' + developer.lastName;
    }

    workDays(maxEstimate) {
        return maxEstimate / 330;
    }

    workLogged() {
        let numberOfDay = 1;
        for (numberOfDay; this.loggedWork <= this.estimate; numberOfDay++) {
            this.loggedWork = this.loggedWork + Math.floor(Math.random() * 300) + 120;
        }
        return numberOfDay;
    }
}
