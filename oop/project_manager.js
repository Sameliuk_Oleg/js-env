import Validator from './validator.js';
export default class Project_manager {
    constructor(firstName, lastName, assignedReleases) {
        Validator.validateProjectManagerName(firstName, 'first name');
        Validator.validateProjectManagerName(lastName, 'last name');

        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedReleases = [];
    }

    assignRelease(release, tasks) {
        Validator.validateReleaseTask(tasks, 'tasks');
        this.assignedReleases.push(release)
    }

    riskFactor(maxEstimate) {
        return maxEstimate = maxEstimate * 0.3 / 330 ;
    }
}
