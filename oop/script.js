import Developer from "./developer.js";
import Task from "./task.js";
import Project_manager from "./project_manager.js";
import Release from "./release.js";


const developer1 = new Developer('Dev1', 'DevLastName1');
const developer2 = new Developer('Dev2', 'DevLastName2');
const task1 = new Task((new Date()).getTime(), 'Task1', 'Description1', 'open', 15156, 0);
const task2 = new Task((new Date()).getTime(), 'Task2', 'Description2', 'open', 11235, 0);
const task3 = new Task((new Date()).getTime(), 'Task3', 'Description3', 'open', 1752, 0);
const task4 = new Task((new Date()).getTime(), 'Task4', 'Description4', 'open', 13648, 0);

const projectFirst = new Release('Project', 'Description')
const projectManager = new Project_manager('ManagerFirstName', 'ManagerLastName');
const timeEstimateProject1 = [projectFirst.maximumTimeLoggedWork('Dev1', 'DevLastName1'), projectFirst.maximumTimeLoggedWork('Dev2', 'DevLastName2')]
const numLoggedDayDev1 = task1.workLogged();
const maxEstimateProject1 = projectFirst.maximumEstimate(timeEstimateProject1);
const riskFactorRelease1 = projectManager.riskFactor(maxEstimateProject1);
const numDayDev1 = task1.workDays(maxEstimateProject1) + riskFactorRelease1;


developer1.assignTask(task1);
task1.assignDeveloper(developer1);
developer2.assignTask(task2);
task2.assignDeveloper(developer2);
developer1.assignTask(task3);
task3.assignDeveloper(developer1);
developer1.assignTask(task4);
task4.assignDeveloper(developer1);
projectFirst.assignProjectManager(projectManager);
projectFirst.taskProject(task1);
projectFirst.taskProject(task2);
projectFirst.taskProject(task4);
projectManager.assignRelease(projectFirst, projectFirst.tasks);

projectFirst.dateRelease(numDayDev1, numLoggedDayDev1);