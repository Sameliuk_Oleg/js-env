import Validator from './validator.js';

export default class Release {
    constructor(name, description, releaseDate, tasks, projectManager) {
        Validator.validateRelease(name, 'name');
        Validator.validateRelease(description, 'description');

        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.tasks = [];
        this.projectManager = projectManager;


    }

    assignProjectManager(ProjectManager) {
        Validator.validateReleaseProjectManager(ProjectManager, 'Project manager')
        this.projectManager = ProjectManager.firstName + " " + ProjectManager.lastName;
    }

    taskProject(task) {
        this.tasks.push(task)
    }

    maximumEstimate(timeEstimateProject) {

        Validator.validateProjectManagerArray(timeEstimateProject, 'array');

        let maximum = timeEstimateProject[0];
        for (let i = 1; i < timeEstimateProject.length; i++) {
            if (maximum < timeEstimateProject[i]) {
                maximum = timeEstimateProject[i]
            }
        }
        return maximum;
    }

    maximumTimeLoggedWork(devName, devLastName) {
        let allTime = 0;
        for (let i = 0; i < this.tasks.length; i++) {
            let developer = devName + ' ' + devLastName;
            (this.tasks[i].assignee === developer) ? allTime += this.tasks[i].estimate : null;
        }
        return allTime;
    }

    dateRelease(dayNumberRelease, daysNumberLogged) {
        let year = 2020;
        let month = 12;
        let day = 5;
        this.releaseDate = new Date(year, month, day);

        for (let i = 1; i < dayNumberRelease; i++) {
            let date = new Date(year, month, day)
            let getDay = date.getDay();
            if (getDay === 6) {
                dayNumberRelease += 2;
            } else if (getDay === 0) {
                dayNumberRelease += 1;
            }
            month++;
            day++;
            if (day > 31) {
                day = 1;
            }
            if ((month > 12) && (day > 31)) {
                month = 1;
            }
        }
        this.releaseDate.setDate(this.releaseDate.getDate() + dayNumberRelease + 7);
        if (this.releaseDate.getDay() === 6) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 2);
        }
        if (this.releaseDate.getDay() === 0) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 1);
        }
        console.log('Data release:', this.releaseDate);

        //real date release  without weekend
        year = 2020;
        month = 12;
        day = 5;
        let dateFixedRelease = new Date(year, month, day);
        for (let i = 1; i < daysNumberLogged; i++) {
            let dateLogged = new Date(year, month, day)
            let getDayLogged = dateLogged.getDay();
            if (getDayLogged === 6) {
                daysNumberLogged += 2;
            } else if (getDayLogged === 0) {
                daysNumberLogged += 1;
            }
            month++;
            day++;
            if (day > 31) {
                day = 1;
                month++;
            }
            if ((month > 12) && (day > 31)) {
                month = 1;
                year++;
            }
        }
        dateFixedRelease.setDate(dateFixedRelease.getDate() + daysNumberLogged);
        if (dateFixedRelease.getDay() === 6) {
            dateFixedRelease.setDate(dateFixedRelease.getDate() + 2);
        }
        if (dateFixedRelease.getDay() === 0) {
            dateFixedRelease.setDate(dateFixedRelease.getDate() + 1);
        }
        console.log('Data fixed release:', dateFixedRelease);

        let dateDelay = daysNumberLogged - dayNumberRelease;
        this.releaseDate.setDate(this.releaseDate.getDate() + dayNumberRelease + dateDelay);

        if (this.releaseDate.getDay() === 6) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 2);
        }
        if (this.releaseDate.getDay() === 0) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 1);
        }
        console.log(this.releaseDate)
    }
}
