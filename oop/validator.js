export class InvalidArgumentError extends Error {
    constructor(message) {
        super();
        this.name = 'InvalidArgumentError';
        this.message = message;
    }
}

export default class Validator {
    static validateDevNameString(value, paramName) {
        if (typeof (value) !== 'string') {
            throw new InvalidArgumentError(
                `'Developer ${paramName} parameter should be string, but ${value} of ${typeof (value)} type received'`
            );
        }
    }

    static validateTaskString(value, paramName) {
        if (typeof (value) !== 'string') {
            throw new InvalidArgumentError(
                `'Task ${paramName} parameter should be string, but ${value} of ${typeof (value)} type received'`
            )
        }
    }

    static validateTaskEstimateNumber(value, paramName) {
        if (typeof (value) !== 'number') {
            throw new InvalidArgumentError(
                `'Task ${paramName} parameter should be number, but ${value} of ${typeof (value)} type received'`
            )
        } else if (value <= 0) {
            throw new InvalidArgumentError(
                `'Task ${paramName} parameter should be more than 0, but ${value} of ${typeof (value)} type received'`
            )
        }
    }

    static validateTaskLoggedWork(value, paramName) {
        if (value < 0) {
            throw new InvalidArgumentError(
                `'Task ${paramName} parameter should be more than 0, but ${value} of ${typeof (value)} type received'`
            )
        }
    }

    static validateProjectManagerName(value, paramName) {
        if (typeof (value) !== 'string') {
            throw new InvalidArgumentError(
                `'Project manager ${paramName} parameter should be string, but ${value} of ${typeof (value)} type received'`
            )
        }
    }

    static validateRelease(value, paramName) {
        if (typeof (value) !== 'string') {
            throw new InvalidArgumentError(
                `'Release ${paramName} parameter should be string, but ${value} of ${typeof (value)} type received'`
            )
        }
    }

    static validateProjectManagerArray(value, paramName){
        if (value.length === 0){
            throw new InvalidArgumentError(
                `'Project manager ${paramName} parameter empty, fill it'`
            )
        }
    }

    static validateReleaseTask(value, paramName){

        if (value.length === 0){
            throw new InvalidArgumentError(
                `'Release ${paramName} parameter empty, fill it'`
            )
        }
    }

    static validateReleaseProjectManager(value, paramName){
        if (typeof (value.firstName) !== 'string' && typeof (value.lastName !== 'string')){
            throw new InvalidArgumentError(
                `'${paramName} parameter empty, fill it'`
            )
        }
    }
}