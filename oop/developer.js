import Validator from './validator.js';
export default class Developer {
    constructor(firstName, lastName, assignedTasks) {
        Validator.validateDevNameString(firstName, 'firstName');
        Validator.validateDevNameString( lastName, 'lastName');

        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedTasks = [];
    }

    assignTask(task) {
        this.assignedTasks.push(task);
    }


}
