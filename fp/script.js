const task = graph => {
    let newDataForAccum = (currentValue) => currentValue.children ? ( searchGraph(currentValue.children)) : '';

    const searchGraph = (graphs) => {
        return graphs.reduce((accumulator, currentValue) => {
            accumulator += currentValue.letter;
            accumulator += newDataForAccum(currentValue);
            return accumulator;
        }, '')
    }

    let sortAccum = (str) => {
        let reduce = (accum, value) => {
            accum.push(str.slice( str.indexOf(value), str.lastIndexOf(value)+1 ));
            return accum;
        }
        let sort = (a,b) => 2 * (a.length < b.length ? 1 : a.length > b.length ? -1 : 0) + (a[0] > b[0] ? 1 : a[0] < b[0] ? -1 : 0);
        return [...new Set(str) ].reduce(reduce, []).sort(sort).map(v => v[0]).join('');
    }

    return sortAccum(searchGraph([graph]).split('').sort());
};
console.log(task(graph));